import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { WalletsController } from './wallets/wallets.controller';
import { WalletsService } from './wallets/wallets.service';
import { WalletsModule } from './wallets/wallets.module';

@Module({
  imports: [WalletsModule],
  controllers: [AppController, WalletsController],
  providers: [AppService, WalletsService],
})
export class AppModule {}
