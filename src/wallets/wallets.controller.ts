import { Controller, Get, Body } from '@nestjs/common';
import { WalletsService } from './wallets.service';

@Controller('wallets')
export class WalletsController {
    constructor(private readonly walletsService: WalletsService) {}

    @Get('checkAddressChecksumEth')
    checkAddressChecksumEth(@Body('address') address: string) {
        return this.walletsService.checkAddressChecksumEth(address);
    }

    @Get('toChecksumAddressEth')
    toChecksumAddressEth(@Body('address') address: string) {
        return this.walletsService.toChecksumAddressEth(address);
    }

    @Get('checkAddressChecksumBtc')
    checkAddressChecksumBtc(@Body('address') address: string) {
        return this.walletsService.checkAddressChecksumBtc(address);
    }

    @Get('checkSignatureEth')
    checkSignatureEth(
        @Body('signature') signature: string, 
        @Body('message') message: string,
        @Body('wallet') wallet: string
    ) {
        return this.walletsService.checkSignatureEth(signature, message, wallet);
    }

    @Get('checkSignatureBtc')
    checkSignatureBtc(
        @Body('signature') signature: string, 
        @Body('message') message: string,
        @Body('wallet') wallet: string
    ) {
        return this.walletsService.checkSignatureBtc(signature, message, wallet);
    }

}
