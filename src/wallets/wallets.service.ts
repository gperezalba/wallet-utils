import { Injectable } from '@nestjs/common';

const Web3 = require('web3');
const validateBtc = require('bitcoin-address-validation');
const bitcoinMessage = require('bitcoinjs-message')

@Injectable()
export class WalletsService {
    private web3: any = new Web3(new Web3.providers.HttpProvider("https://connect.pichain.io"));

    checkAddressChecksumEth(address: string): boolean {
        return this.web3.utils.checkAddressChecksum(address);
    }

    toChecksumAddressEth(address: string): string {
        //Eliminar caracteres de control
        address = address.replace(/[\x00-\x1F\x7F-\x9F]/g, "");
        //Eliminar espacios en blanco
        address = address.replace(/\s/g, "");
        //Corregir checksum (problemas con mayúsculas minúsculas etc)
        return this.web3.utils.toChecksumAddress(address);
    }

    checkAddressChecksumBtc(address: string): boolean {
        let result = validateBtc(address);

        if (!result) {
            return false;
        } else {
            return true;
        }
    }

    //ESTO ES PARA EL FORMULARIO DEL DEX
    //El mensaje debe ser el wallet del usuario concatenado con algo aleatorio
    async signMessageEth(message: string, wallet: string): Promise<string> {
        let signature = await this.web3.eth.personal.sign(message, wallet);
        return signature;
    }

    //Se le pasa la firma, el mensaje que se supone a firmado y se checkea que el 
    //wallet que ha firmado es el que queremos
    async checkSignatureEth(signature: string, message: string, wallet: string): Promise<boolean> {
        let signer = await this.web3.eth.personal.ecRecover(message, signature);

        signer = this.toChecksumAddressEth(signer);
        wallet = this.toChecksumAddressEth(wallet);

        if (signer == wallet) {
            return true;
        } else {
            return false;
        }
    }

    checkSignatureBtc(signature: string, message: string, wallet: string): boolean {
        return bitcoinMessage.verify(message, wallet, signature);
    }
}
