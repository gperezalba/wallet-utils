curl -X GET -H "Content-Type: application/json" -d '{"address": "0x4946E29F90980aBcd4C2554c657beDA308aeC75A"}' http://localhost:3000/wallets/checkAddressChecksumEth
curl -X GET -H "Content-Type: application/json" -d '{"address": "0xc1912fee45d61c87cc5ea59dae31190fffff232d"}' http://localhost:3000/wallets/checkAddressChecksumEth
curl -X GET -H "Content-Type: application/json" -d '{"address": "0xc1912fee45d61c87cc5ea59dae31190fffff232d"}' http://localhost:3000/wallets/toChecksumAddressEth
curl -X GET -H "Content-Type: application/json" -d '{"address": "0XC1912FEE45D61C87CC5EA59DAE31190FFFFF232D"}' http://localhost:3000/wallets/toChecksumAddressEth
curl -X GET -H "Content-Type: application/json" -d '{"address": "0xc1912fEE45d61C87Cc5EA59DaE31190FFFFf232d"}' http://localhost:3000/wallets/checkAddressChecksumEth
curl -X GET -H "Content-Type: application/json" -d '{"address": "1BV8pPSgHPEKEwj7qS3yfi1rvJDGkqJo7u"}' http://localhost:3000/wallets/checkAddressChecksumBtc
curl -X GET -H "Content-Type: application/json" -d '{"address": "bc1qrwaf9fmqdvpmmwpxkqre7na35w0wxqskrf7s62"}' http://localhost:3000/wallets/checkAddressChecksumBtc
curl -X GET -H "Content-Type: application/json" -d '{"address": "3H7kNSYDn77bBG9aQ6FQ4Wd7VZ7srvmqRD"}' http://localhost:3000/wallets/checkAddressChecksumBtc
curl -X GET -H "Content-Type: application/json" -d '{"address": "12EJmB3cMGRNveskzA7g7kxW32gSbo2dHF"}' http://localhost:3000/wallets/checkAddressChecksumBtc

curl -X GET -H "Content-Type: application/json" -d '{"signature": "0x6d1a146394e46601f4406d753c36c1608fee1ca41d2937517adff364ba064b211d6985f56096c38e85aa4e6639a74b957b73730291187b3e33f888efc746b3921b", "message": "Some string", "wallet": "0x4946e29f90980abcd4c2554c657beda308aec75a"}' http://localhost:3000/wallets/checkSignatureEth
curl -X GET -H "Content-Type: application/json" -d '{"signature": "H3OvI1tMNpegCF0rKcnynx7mjS20NOSbkq82VknvDSYyajrs/6AOF4oo0CKWWvKhKnaBlhhNhLfhnSovQrRrtL4=", "message": "Some string", "wallet": "1Q4soZXLqXvLXtqH5USRQm5GaS8fFrTUv4"}' http://localhost:3000/wallets/checkSignatureBtc